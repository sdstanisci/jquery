$(document).ready(function(){
    $(".slider").slick({
      dots: true,
      infinite: true,
      speed: 300,
      slidesToDown: 1,
      adaptiveHeight: true
    });
});
