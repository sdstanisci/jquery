$("#botao-placar").click(mostraPlacar);
$("#botao-sync").click(sincronizaPlacar);


function atualizaPlacar(){
  $.get("http://localhost:3000/placar", function(data){
    //envolvendo com a funcao jquery, ganha os poderes de jquery
    $(data).each(function(){
      var linha = novaLinha(this.usuario, this.pontos);
      linha.find(".botao-remover").click(removeLinha);
      $("tbody").append(linha);
    });
  });
}

function sincronizaPlacar(){
  var placar = [];
  //seletor que pega todas as tr de tbody
  var linhas = $("tbody>tr");

  // funcao each eh parecido com o foreach porem ele le os subelemtos
  linhas.each(function(){
    //seletor css td:nth-child pega a td de acordo com a posicao informada no parametro
    var usuario = $(this).find("td:nth-child(1)").text();
    var palavras = $(this).find("td:nth-child(2)").text();
    // obj js
    var score = {
      usuario: usuario,
      pontos: palavras
    };
    placar.push(score);
  });
  //quando vai se fazer um post precisa mandar um string ou obj js
  var dados = {
    placar: placar
  };
  $.post("http://localhost:3000/placar", dados, function(){
    console.log("salvouuuuu");
    $(".tooltip").tooltipster("open").tooltipster("content","sucessoooo ao sincronizar");
  }).fail(function(){
    $(".tooltip").tooltipster("open").tooltipster("content","falha ao sincronizar");
  }).always(function(){
    setTimeout(function(){
      $(".tooltip").tooltipster("close");
    },1200);
  });
}

function mostraPlacar(){

  // com o stop ele para a animacao
  $(".placar").stop().slideToggle(2000);
  //Exibe e esconde o elemteno pode ser usado o hide ou show
  //$(".placar").toggle();

  //Pequena animacao para exibir elemento na pagina
  //$(".placar").slideDown(2000);
  //$(".placar").slideUp(2000);
}

function inserePlacar(){
   var corpoTabela = $(".placar").find("tbody");
   var usuario = $("#usuarios").val();
   var numPalavras = $("#contador-palavras").text();

   var linha = novaLinha(usuario, numPalavras);
   linha.find(".botao-remover").click(removeLinha);

   corpoTabela.append(linha);
   $("#placar").slideDown(500);
   scrollPlacar();
}

function scrollPlacar(){
  var posicaoPlacar = $(".placar").offset().top;
  $("body").animate({
    scrollTop: posicaoPlacar+"px"
  },1000);
}

function novaLinha(usuario, palavras){
      var linha = $("<tr>");
      var colunaUsuario = $("<td>").text(usuario);
      var colunaPalavras = $("<td>").text(palavras);
      var colunaRemover = $("<td>");

      var link = $("<a>").attr("href","#").addClass("botao-remover");
      var icone = $("<i>").addClass("small").addClass("material-icons").text("delete");

      link.append(icone);

      colunaRemover.append(link);

      linha.append(colunaUsuario);
      linha.append(colunaPalavras);
      linha.append(colunaRemover);

      return linha;
}

function removeLinha(event){
    event.preventDefault();

    var linha = $(this).parent().parent();
    // eveito para diminuir ate sumir,fica como display none
    linha.fadeOut(2000);

    //segura o fadeOut e para depois remover
    setTimeout(function(){
      //remove sem animacao
      linha.remove();
    },2000);


}
