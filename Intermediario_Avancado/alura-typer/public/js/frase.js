$("#botao-frase").click(fraseAleatoria);
$("#botao-frase-id").click(buscaFrase);

function buscaFrase(){
  var fraseId = $("#frase-id").val();
  var dados = {id: fraseId};
  // vai enviar a variavel dados e retornar a funcao trocaFrase
  $("#spinner").show();
  $.get("http://localhost:3000/frases", dados, trocaFrase)
  .fail(function(){ // em caso de erro
    $("#erro").toggle();
    setTimeout(function(){
      $("#erro").toggle();
    },1000);
  })
  .always(function(){ //sempre executa independente se der certo ou errado a requisição
    $("#spinner").hide();
  });
}

function trocaFrase(data){
  var frase = $(".frase");
  frase.text(data.texto);
  atualizaTamanhoFrase();
  atualizaTempoInicial();
}

function fraseAleatoria(){
  $("#spinner").show();

  $.get("http://localhost:3000/frases",trocaFraseAleatorio)
   .fail(function(){ // em caso de erro
     $("#erro").toggle();
     setTimeout(function(){
       $("#erro").toggle();
     },1000);
   })
   .always(function(){ //sempre executa independente se der certo ou errado a requisição
     $("#spinner").hide();
   });
}

function trocaFraseAleatorio(data){
  var frase = $(".frase");
  var numeroAleatorio = Math.floor(Math.random() * data.length);
  frase.text(data[numeroAleatorio].texto);
  atualizaTamanhoFrase();
  atualizaTempoInicial(data[numeroAleatorio].tempo);
}
