var tempoInicial = $("#tempo-digitacao").text();
var campo = $(".campo-digitacao");

<!-- pode usar direto $(function(){}); -->
$(document).ready(function(){
  atualizaTamanhoFrase();
  inicilizaContadores();
  inicializaCronometro();
  inicializaMarcadores();
  $("#botao-reiniciar").click(reiniciaJogo);
});

function atualizaTamanhoFrase(){
  var frase = $(".frase").text();
  var numPalavras = frase.split(" ").length;
  var tamanhoFrase = $("#tamanho-frase");
  tamanhoFrase.text(numPalavras);
}

function inicilizaContadores(){
  campo.on("input", function(){
    var conteudo = campo.val();
    var qntPalavras = conteudo.split(/\S+/).length-1;

    $("#contador-palavras").text(qntPalavras);

    var qntCaracteres = conteudo.length;
    $("#contador-caracteres").text(qntCaracteres);

  });
}

function inicializaCronometro(){
  var tempoRestante = $("#tempo-digitacao").text();
  campo.one("focus",function(){
    var cronometroId = setInterval(function(){
      if(tempoRestante==0){
        clearInterval(cronometroId);
        finalizaJogo();
      }
      $("#tempo-digitacao").text(tempoRestante--);

    }, 1000);
  });
}

function inicializaMarcadores(){
  var frase = $(".frase").text();
  campo.on("input",function(){
    var digitado = campo.val();
    var comparavel = frase.substr(0, digitado.length);

    if(digitado == comparavel){
      campo.addClass("borda-verde");
      campo.removeClass("borda-vermelha");
    }else {
      campo.removeClass("borda-verde");
      campo.addClass("borda-vermelha");
    }
  });
}

function reiniciaJogo(){
  campo.attr("disabled", false);
  campo.val("");
  $("#contador-palavras").text("0");
  $("#contador-caracteres").text("0");
  $("#tempo-digitacao").text(tempoInicial);
  inicializaCronometro();
  <!-- campo.removeClass("campo-desativado"); -->
  campo.toggleClass("campo-desativado");
  campo.removeClass("borda-verde");
  campo.removeClass("borda-vermelha");
}

function finalizaJogo(){
  <!-- campo.css("background-color","lightgray"); -->
  <!-- campo.addClass("campo-desativado"); -->
  <!-- toggleClass eh uma chave de liga/desliga para nao precisar ficar fazendo addClass ou removeClass -->
  campo.attr("disabled", true);
  campo.toggleClass("campo-desativado");
  inserePlacar();
}
