
		var atualizaDados = function(){
			var itens = $(".item-total");
			var total = 0;

			for(var i=0; i < itens.length; i++ ){
				var item = $(itens[i]);
				var valor = parseFloat(item.text());
				total = total + valor;
			}
			$("#valor-total").text(total);
			$("#quantidade-de-itens").text(itens.length);

			<!-- var atual = parseInt($("#quantidade-de-itens").text()); -->
			<!-- var novaQuantidade = atual - 1; -->
			<!-- $("#quantidade-de-itens").text(novaQuantidade); -->
			<!-- pega o pai do pai-->
			<!-- self.parent().parent().remove(); -->
			<!-- Pega o Pai do Pai, o closest navega na arvore ate achar o elemento-->

			<!-- var valorTotal = parseFloat($("#valor-total").text()); -->
			<!-- var valorAtual = parseFloat(self.closest("tr").find(".item-total").text()); -->
			<!-- var valorFinal = parseFloat(valorTotal - valorAtual); -->
			<!-- $("#valor-total").text(valorFinal); -->
		};

		var removerItem = function(event){
			event.preventDefault();
			var self = $(this);
			<!-- self.closest("tr").remove(); -->
      self.closest("tr").hide();
			atualizaDados();
		};

   var undo = function(){
     $("tr:visible").removeClass("recuperados");

     var trs = $("tr:hidden");
     trs.addClass("recuperados").show();
   };

	 var aposInicializado = function(){
		 atualizaDados();
     $("#undo").click(undo);
		 $(".remove-item").click(removerItem);
	 };

	 $(aposInicializado);
